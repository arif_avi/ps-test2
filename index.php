<?php
/**
 * https://gist.github.com/mariusbalcytis/205de9e4ef7d29fc051f4e572bb30184
 */
class Task {    
    var $range = 20;
    var $delimiter = ' ';
    var $mod1 = null;
    var $mod2 = null;
    var $modText = array();
    var $output = array();
    var $checkNumber = array();
    
    function __construct($numberRange, $delimiter, $mod1, $mod2, $modText, $checkNumber = array()) 
    {
        $this->range = $numberRange;
        $this->delimiter = $delimiter;
        $this->mod1 = $mod1;
        $this->mod2 = $mod2;
        $this->modText = $modText;
        $this->checkNumber = $checkNumber;
    }
    
    function executeTask()
    {
        $i = 0;
        while($i < $this->range){
            $i++;
            
            if($this->checkTask3($i)){
                continue;
            }
            
            if($i % $this->mod1 == 0 && $i % $this->mod2 === 0){
                $this->output[] = $this->modText[2];
                continue;
            }

            if($i % $this->mod1 === 0){
                $this->output[] = $this->modText[0];
            }        
            elseif($i % $this->mod2 === 0){
                $this->output[] = $this->modText[1];
            }
            else {
                $this->output[] = $i;            
            }
        }
        return implode($this->delimiter, $this->output);
    }    
    
    function checkTask3($number)
    {
        if(!is_array($this->mod1)){
            return false;
        }
        //if mod1 is array 1,4,9 then check if $i 
        if(in_array($number, $this->mod1) && $number <= $this->mod2){
            $this->output[] = $this->modText[0];
        }
        
        // > than mod2
        if(!in_array($number, $this->mod1) && $number > $this->mod2){
            $this->output[] = $this->modText[1];
        }
        
        if($number > $this->mod2 && in_array($number, $this->mod1)){
            $this->output[] = $this->modText[2];
        }
        
        if(!in_array($number, $this->mod1) && $number <= $this->mod2){
            $this->output[] = $number;
        }
        
        return true;
    }
    
}

$modText = array(
    "pa", "pow","papow"
);
$task = new Task(20, " ", 3, 5, $modText);
echo $task->executeTask().PHP_EOL;


$modText = array(
    "hatee", "ho","hateeho"
);
$task = new Task(15, "-", 2, 7, $modText);
echo $task->executeTask().PHP_EOL;

$modText = array(
    "joff", "tchoff","jofftchoff"
);
$task = new Task(10, "-", array(1,4,9), 5, $modText);
echo $task->executeTask().PHP_EOL;